package cz.petrsokola.GpsTransportMode;

public abstract class InnerFeature {
    protected String description;
    @Override
    public String toString()
    {
	return description;
    }
}
