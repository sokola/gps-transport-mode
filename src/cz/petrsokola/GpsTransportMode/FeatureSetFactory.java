package cz.petrsokola.GpsTransportMode;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import cz.petrsokola.GpsTransportMode.WindowSample.Statistics;

public class FeatureSetFactory {
    public static final int NEIGHBOURS_EACH_SIDE = 2;

    private List<Feature> features;
    private int[] neighbourIdx;
    
    // Vyr�b� seznam v�ech mo�n�ch featur.
    // D�le pot�eba pou��vat pr�v� tyto instance
    private List<Feature> createFeatures() {
	IntervalInnerFeature[] tmpArr;

	double[][] speedIntervals = { { 0.0, 2.0 }, { 1.0, 4.0 }, { 3.0, 7.0 }, { 5.5, 12.5 }, { 9.7, 22.0 },
		{ 13.8, 30.0 }, { 22.2, 41.6 }, { 33.3, 47.2 }, { 45.8, Double.POSITIVE_INFINITY } };
	// vytvo�it vnit�n� featury rychlosti
	tmpArr = createIntervalInnerFeatures(speedIntervals, SpeedInnerFeature.class);
	SpeedInnerFeature[] speedFeatures = Arrays.copyOf(tmpArr, tmpArr.length, SpeedInnerFeature[].class);

	double[][] accIntervals = { { Double.NEGATIVE_INFINITY, -1.0 }, { -1.5, -0.3 }, { -0.5, -0.05 }, { -0.1, 0.1 },
		{ 0.05, 0.5 }, { 0.3, 1.5 }, { 1.0, Double.POSITIVE_INFINITY } };
	// vytvo�it vnit�n� featury zrychlen�
	tmpArr = createIntervalInnerFeatures(accIntervals, AccelerationInnerFeature.class);
	AccelerationInnerFeature[] accFeatures = Arrays.copyOf(tmpArr, tmpArr.length, AccelerationInnerFeature[].class);

	double[][] turnIntervals = { { Double.NEGATIVE_INFINITY, 2e-5 }, { 1e-5, 2e-4 }, { 1e-4, 2e-3 }, { 1e-3, 2e-2 },
		{ 1e-2, 2e-1 }, { 1e-1, 2 }, { 1, 20 }, { 10, Double.POSITIVE_INFINITY } };
	// vytvo�it vnit�n� featury zat��en�
	tmpArr = createIntervalInnerFeatures(turnIntervals, TurnrateInnerFeature.class);
	TurnrateInnerFeature[] turnFeatures = Arrays.copyOf(tmpArr, tmpArr.length, TurnrateInnerFeature[].class);
	
	List<Feature> features = new LinkedList<Feature>();
	// rozn�sobit skrze mno�inu soused� a (min, avg, max)
	for (int idx : this.neighbourIdx) {
	    multiplyWithOneIndex(speedFeatures, idx, features);
	    multiplyWithOneIndex(accFeatures, idx, features);
	    multiplyWithOneIndex(turnFeatures, idx, features);
	}

	return features;
    }

    private void multiplyWithOneIndex(InnerFeature[] innerFeatures, int index, List<Feature> features) {
	for (InnerFeature innf : innerFeatures) {
	    for (StatisticsValueKind stats : StatisticsValueKind.values())
		features.add(new Feature(innf, index, stats));
	}
    }

    @SuppressWarnings("rawtypes")
    private IntervalInnerFeature[] createIntervalInnerFeatures(double[][] intervals, Class intervalClass) {
	int n = intervals.length;
	IntervalInnerFeature[] features = new IntervalInnerFeature[n];

	for (int i = 0; i < n; i++) {
	    try {
		// konstruktor t� spr�vn� t��dy
		@SuppressWarnings({ "unchecked" })
		Constructor c = intervalClass.getConstructor(new Class[] { double.class, double.class });
		features[i] = (IntervalInnerFeature) (c.newInstance(intervals[i][0], intervals[i][1]));
	    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
		    | InvocationTargetException | NoSuchMethodException | SecurityException e) {
		e.printStackTrace();
	    }
	}
	return features;

    }

    public FeatureSetFactory() {
	// p�ipravit indexy pro ur�en� sousednosti oken
	this.neighbourIdx = new int[2 * NEIGHBOURS_EACH_SIDE + 1];
	for (int i = 1; i <= NEIGHBOURS_EACH_SIDE; i++) {
	    neighbourIdx[i] = i;
	    neighbourIdx[i + NEIGHBOURS_EACH_SIDE] = -i;
	}
	neighbourIdx[0] = 0;
	
	this.features = createFeatures();
    }    

    public List<Feature> getAllFeatures() {
	return this.features;
    }

    public String getAllFeaturesDescriptions() {
	String s = "";
	int i = 1;
	for (Feature f : features) {
	    s += (i++) + ": " + f.toString() + System.lineSeparator();
	}
	return s;
    }
    
    public List<Feature> getFeatures(List<WindowSample> windowList, int index)
    {
	List<Feature> list = new LinkedList<Feature>();
	for (int i: neighbourIdx)
	{
	    if (index + i >= 0 && index + i < windowList.size())
	    {
		WindowSample w = windowList.get(index + i);
		addFeaturesFromOneWindow(w, i, list);
	    }
	}
	return list;
    }

    // p�ekl�d� parametry okna na featury
    // zapouzd�en� v oknu a ve featu�e je navz�jem ortogon�ln� (p�ek��en�
    // vn�j��-vnit�n�)
    private void addFeaturesFromOneWindow(WindowSample w, int neighbourIndex, List<Feature> listTo) {
	for (Feature f : features) {
	    if (f.getWindowRelativePosition() == neighbourIndex) {
		InnerFeature innf = f.getInnerFeature();
		// um�me zpracovat intervalov� featury
		if (innf instanceof IntervalInnerFeature) {
		    IntervalInnerFeature intf = (IntervalInnerFeature) innf;
		    StatisticsValueKind statsKind = f.getStatsKind();
		    Statistics stats;
		    // na featu�e rozli�uju veli�inu, z okna z�sk�m statistiku
		    // veli�iny
		    if (intf instanceof SpeedInnerFeature)
			stats = w.speed;
		    else if (intf instanceof AccelerationInnerFeature)
			stats = w.acceleration;
		    else // TurnrateInnerFeature)
			stats = w.turnRate;
		    // na statistice okna beru stat.ukazatel podle stat.typu
		    // featury
		    double windowValue;
		    switch (statsKind) {
		    case AVG:
			windowValue = stats.avg;
			break;
		    case MAX:
			windowValue = stats.max;
			break;
		    case MIN:
			windowValue = stats.min;
			break;
		    case STDEV:
			windowValue = stats.stDeviation;
			break;
		    default:
			windowValue = Double.NaN;
			break;
		    }
		    // m�me hodnotu z okna a interval z featury a v�me, �e jsou
		    // spr�vn�ho
		    // - relativn�ho posunu
		    // - veli�iny
		    // - statistick�ho ukazatele
		    // porovn�me, zda padne do intervalu, pokud ano, pak okno m�
		    // tuto featuru

		    if (intf.getMin() <= windowValue && intf.getMax() >= windowValue)
			listTo.add(f);
		}
	    }
	}
    }
}
