package cz.petrsokola.GpsTransportMode;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

import de.daslaboratorium.machinelearning.classifier.Classifier;

public class LearnProvider {

    private SampleSerializer serializer;
    private FeatureSetFactory featureSetFactory;

    public LearnProvider(FeatureSetFactory featureSetFactory) {
	this.serializer = new SampleSerializer();
	this.featureSetFactory = featureSetFactory;
    }

    public void LearnFromDirectory(File directory, Classifier<Feature, TransportCategory> classifier)
	    throws IOException {
	if (directory.isDirectory()) {
	    FileFilter filter = new FileFilter() {

		@Override
		public boolean accept(File pathname) {
		    return pathname.isFile() && pathname.getName().endsWith("xml");
		}
	    };
	    File[] files = directory.listFiles(filter);
	    
	    for(File f: files)
	    {
		List<WindowSample> samples = serializer.deserializeListFromFile(f);
		
		for(int i = 0; i<samples.size(); i++)
		{
		    WindowSample ws = samples.get(i);
		    TransportCategory category = ws.transportType;
		    List<Feature> features = featureSetFactory.getFeatures(samples, i);
		    classifier.learn(category, features);		    
		}		
	    }

	} else
	    throw new IOException("File is not a directory");

    }

}
