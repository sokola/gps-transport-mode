package cz.petrsokola.GpsTransportMode;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.TypeCheckError;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class SampleSerializer {
    private XStream xStream = new XStream(new DomDriver());

    public void serializeOneToFile(WindowSample sample, File file) throws TypeCheckError, IOException {
	if (sample instanceof WindowSample)
	    serializeToFile(sample, file);
	else
	    throw new TypeCheckError("Instance of WindowSample expected.", sample);
    }

    public void serializeListToFile(List<WindowSample> list, File file) throws TypeCheckError, IOException {
	if (list instanceof List<?>)
	    serializeToFile(list, file);
	else
	    throw new TypeCheckError("Instance of List<WindowSample> expected.", list);
    }

    private void serializeToFile(Object o, File file) throws IOException {
	FileWriter fw = new FileWriter(file);
	xStream.toXML(o, fw);
	fw.close();
    }

    private Object deserializeFromFile(File file) throws IOException {
	FileReader fr = new FileReader(file);
	Object o = xStream.fromXML(fr);
	fr.close();
	return o;
    }

    public WindowSample deserializeOneFromFile(File file) throws ClassCastException, IOException {
	return (WindowSample) deserializeFromFile(file);
    }
    
    @SuppressWarnings("unchecked")
    public List<WindowSample> deserializeListFromFile(File file) throws IOException
    {
	return (List<WindowSample>) deserializeFromFile(file);
    }

}
