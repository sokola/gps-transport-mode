package cz.petrsokola.GpsTransportMode;

import java.util.List;
import org.gavaghan.geodesy.*;

public class GeoPreprocessor {

    private GeodeticCalculator gc = new GeodeticCalculator();
    private Ellipsoid e = Ellipsoid.WGS84;

    public GeoPreprocessor() {
    }

    public void processList(List<TrackPoint> points) {
	TrackPoint prev = null;
	TrackPoint curr = null;

	GeodeticCurve curve;

	int n = points.size();
	int i;
	for (i = 1; i < n; i++) {
	    if (curr == null)
		prev = points.get(0);
	    else
		prev = curr;
	    curr = points.get(i);

	    curve = diffData(prev, curr);
	    curr.setDt((int) ((curr.getTimestamp().getTime() - prev.getTimestamp().getTime())/1000));
	    curr.setDxy(curve.getEllipsoidalDistance());
	    curr.setDirection(curve.getAzimuth());

	    curr.setSpeed(curr.getDxy() / curr.getDt());
	    curr.setAcceleration((curr.getSpeed() - prev.getSpeed()) / curr.getDt());
	    // div by 0 possible -> patched in setTurnRate
	    curr.setTurnRate(Math.abs(curr.getDirection() - prev.getDirection()) / curr.getDxy());
	}
    }

    private GeodeticCurve diffData(TrackPoint a, TrackPoint b) {
	GlobalCoordinates ca = new GlobalCoordinates(a.getLat(), a.getLon());
	GlobalCoordinates cb = new GlobalCoordinates(b.getLat(), b.getLon());
	return gc.calculateGeodeticCurve(e, ca, cb);
    }
}
