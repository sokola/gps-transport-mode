package cz.petrsokola.GpsTransportMode;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.TypeCheckError;

import de.daslaboratorium.machinelearning.classifier.bayes.BayesClassifier;

public class GpxClassifier {

    private BayesClassifier<Feature, TransportCategory> classifier;
    private FeatureSetFactory featureSetFactory;
    private LearnProvider learnProvider;

    public GpxClassifier() {
	this.classifier = new BayesClassifier<Feature, TransportCategory>();
	this.featureSetFactory = new FeatureSetFactory();
	this.learnProvider = new LearnProvider(featureSetFactory);
    }

    private List<WindowSample> processGpxFile(File gpx) {
	try {
	    GpxParser gp = new GpxParser(gpx);
	    List<TrackPoint> points = gp.getPoints();
	    GeoPreprocessor gpre = new GeoPreprocessor();
	    gpre.processList(points);
	    List<WindowSample> samples = new WindowSampleFactory().getWindowSamples(points);
	    return samples;
	} catch (ParserConfigurationException | SAXException | IOException | ParseException e) {
	    System.err.println("Error processing gpx file:" + System.lineSeparator() + e.getLocalizedMessage());
	    return null;
	}

    }
    
    private void classify(File gpx)
    {
	List<WindowSample> samples = processGpxFile(gpx);
	List<Feature> features;
	WindowSample sample;
	TransportCategory category;
	for (int i = 0; i < samples.size(); i++)
	{
	    sample = samples.get(i);
	    features = featureSetFactory.getFeatures(samples, i);
	    category = classifier.classify(features).getCategory();
	    
	    System.out.println(sample.toString());
	    System.out.println(category.toString());
	}	
	
    }

    private void menu() {
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	printHelp();
	String s;
	String[] tokens;
	while (true) {
	    try {
		s = br.readLine();
		tokens = s.split("\\s");
		try {
		    switch (tokens[0]) {
		    case "t":
			preprocess(new File(tokens[1]), new File(tokens[2]));
			System.out.println("Preprocessing the file OK.");
			break;
		    case "l":
			learnProvider.LearnFromDirectory(new File(tokens[1]), classifier);
			System.out.println(String.format("Learned %d samples, knowing of %d categories.",
				classifier.getCategoriesTotal(), classifier.getCategories().size()));
			break;
		    case "r":
			classifier.reset();
			System.out.println("Reset OK.");
			break;
		    case "c":
			classify(new File(tokens[1]));
			break;		    
		    case "h":
			printHelp();
			break;
		    case "q":
			System.exit(0);
			break;
		    case "g":
			for (TransportCategory cat: TransportCategory.values())
			System.out.println(cat.toString());
			break;
		    default:
			System.err.println("Unrecognized command. Please try again.");
			printHelp();
			break;
		    }
		} catch (ArrayIndexOutOfBoundsException e) {
		    System.err.println("Wrong number of parameters. Please try again.");
		    printHelp();
		}
	    } catch (IOException e) {
		System.err.println("IO Exception:" + System.lineSeparator() + e.getLocalizedMessage());
	    }
	}
    }

    private void preprocess(File gpx, File xml) {
	List<WindowSample> samples = processGpxFile(gpx);
	SampleSerializer serializer = new SampleSerializer();
	try {
	    serializer.serializeListToFile(samples, xml);
	} catch (TypeCheckError | IOException e) {
	    System.err.println("Error while creating xml file:");
	    System.err.println(e.getLocalizedMessage());
	}
    }

    private void printHelp() {
	System.out.println("Please type one of following commands.");
	System.out.println(
		"t <path-to-file> <path-to-file>   # Converts a gpx file given in first argument to window samples and saves them to a file given in second argument. Useful for following manual tagging of the training data. The resulting XML document has transport category of UNRECOGNIZED_OR_MULTIPLE.");
	System.out.println(
		"l <directory>   # Searches for tagged *.xml files in given directory and uses them to learn the classifier. Calling repeatedly, it does not forget previous knowledge.");
	System.out.println("r   # Resets the classifier memory. Forgets what has learned.");
	System.out.println(
		"c <path-to-file>   # Classifies the track in the given gpx file. Outputs one most probable category.");
	System.out.println("g   # Prints out all distinguishable categories.");
	System.out.println("h   # See this help.");
	System.out.println("q   # Quit.");
    }

    public static void main(String[] args) {
	new GpxClassifier().menu();
    }

}
