package cz.petrsokola.GpsTransportMode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WindowSample {

    public class Statistics {
	public double min;
	public double max;
	public double avg;
	public double stDeviation;
	
	public String toString()
	{
	    return "\t(min "+ min+"; max "+max+"; avg "+avg+"; sd "+stDeviation+")";
	}
    }

    public Statistics speed = new Statistics();
    public Statistics acceleration = new Statistics();
    public Statistics turnRate = new Statistics();
    
    public Date startTime;
    public Date endTime;

    public String getDescription() {
	DateFormat df = new SimpleDateFormat();
	return df.format(startTime) + System.lineSeparator()
		+ speed.toString() + System.lineSeparator()
		+ acceleration.toString() + System.lineSeparator()
		+ turnRate.toString() + System.lineSeparator()
		+ "assigned transport type: " + (transportType == null ? "<null>" : 
		transportType.toString()) + System.lineSeparator()
		+ df.format(endTime) + System.lineSeparator();
    }
    
    public String toString(){
	DateFormat df = new SimpleDateFormat();
	return "Window sample" + System.lineSeparator()
		+"\t" + df.format(startTime) + System.lineSeparator()
		+"\t" + df.format(endTime);
    }
    
    public TransportCategory transportType = TransportCategory.UNRECOGNIZED_OR_MULTIPLE;

}
