package cz.petrsokola.GpsTransportMode;

public final class Feature {
    private InnerFeature innerFeature;
    private int windowRelativePosition;
    private StatisticsValueKind statsKind;

    public Feature(InnerFeature innerFeature, int relativePos, StatisticsValueKind statsKind) {
	this.innerFeature = innerFeature;
	this.windowRelativePosition = relativePos;
	this.statsKind = statsKind;
    }

    @Override
    public String toString() {
	if (windowRelativePosition == 0)
	    return "This window: " + statsKind.toString() +" "+ innerFeature.toString();
	else
	    return String.format("Neighbour (%d): ", windowRelativePosition) + statsKind.toString() +" "+ innerFeature.toString();
    }

    public InnerFeature getInnerFeature() {
        return innerFeature;
    }

    public int getWindowRelativePosition() {
        return windowRelativePosition;
    }

    public StatisticsValueKind getStatsKind() {
        return statsKind;
    }

}
