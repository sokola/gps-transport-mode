package cz.petrsokola.GpsTransportMode;

public final class SpeedInnerFeature extends IntervalInnerFeature {
    public SpeedInnerFeature(double min, double max)
    {
	super(min, max);
    }

    @Override
    protected String setDescription() {
	return String.format("Speed is between %.2f and %.2f km/h.", min*3.6, max*3.6);
    }

}
