package cz.petrsokola.GpsTransportMode;

public final class AccelerationInnerFeature extends IntervalInnerFeature {

    public AccelerationInnerFeature(double min, double max) {
	super(min, max);
    }

    @Override
    protected String setDescription() {
	return String.format("Acceleration is between %.3f and %.3f g.", min/9.81, max/9.81);
    }

}
