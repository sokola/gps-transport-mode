package cz.petrsokola.GpsTransportMode;

public abstract class IntervalInnerFeature extends InnerFeature{
    protected double min;
    protected double max;
    protected IntervalInnerFeature(double min, double max)
    {
	this.min = min;
	this.max = max;
	this.description = setDescription();
    }
    
    protected abstract String setDescription();

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }
}
