package cz.petrsokola.GpsTransportMode;

public class TurnrateInnerFeature extends IntervalInnerFeature {

    public TurnrateInnerFeature(double min, double max) {
	super(min, max);
    }

    @Override
    protected String setDescription() {
	return String.format("Turn rate is between %.4f and %.4f.", min, max);
    }

}
