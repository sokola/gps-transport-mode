package cz.petrsokola.GpsTransportMode;

public enum TransportCategory {
    WALK,
    RUN,
    BIKE,
    CITY_BUS,
    CITY_TRAM,
    CITY_CAR,
    HIGHWAY_CAR,
    LOCAL_TRAIN,
    FAST_TRAIN,
    SHIP,
    PLANE,
    STILL,
    UNRECOGNIZED_OR_MULTIPLE
}
