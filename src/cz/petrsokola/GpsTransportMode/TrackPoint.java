package cz.petrsokola.GpsTransportMode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrackPoint {
    private double lat;
    private double lon;
    private Date timestamp;

    private int dt; // time difference
    private double dxy; // position difference in meters
    private double speed; // in m/s
    private double direction; // 0..360 to the north
    private double turnRate; // direction derivative /dxy
    private double acceleration; // speed derivative /dt

    public TrackPoint(double lat, double lon, Date timestamp) {
	this.lat = lat;
	this.lon = lon;
	this.timestamp = timestamp;
    }

    public double getLat() {
	return lat;
    }

    public double getLon() {
	return lon;
    }

    public Date getTimestamp() {
	return timestamp;
    }

    @Override
    public String toString() {
	DateFormat df = new SimpleDateFormat("hh:mm:ss");
	return ("[" + this.lat + "; " + this.lon + "]@" + df.format(this.timestamp)) + System.lineSeparator()
		+ "\ttimediff " + this.dt + System.lineSeparator() + "\tdistance " + this.dxy + System.lineSeparator()
		+ "\tspeed " + this.speed + System.lineSeparator() + "\tdirection " + this.direction
		+ System.lineSeparator() + "\tacceleration " + this.acceleration + System.lineSeparator()
		+ "\tturnrate " + this.turnRate;
    }

    public double getSpeed() {
	return speed;
    }

    public void setSpeed(double speed) {
	this.speed = speed;
    }

    public double getDirection() {
	return direction;
    }

    public void setDirection(double direction) {
	this.direction = direction;
    }

    public double getTurnRate() {
	return turnRate;
    }

    public void setTurnRate(double turnRate) {
	if (Double.isNaN(turnRate))
	    this.turnRate = 0.0;
	else
	    this.turnRate = turnRate;
    }

    public double getAcceleration() {
	return acceleration;
    }

    public void setAcceleration(double acceleration) {
	this.acceleration = acceleration;
    }

    public double getDt() {
	return dt;
    }

    public void setDt(int dt) {
	this.dt = dt;
    }

    public double getDxy() {
	return dxy;
    }

    public void setDxy(double dxy) {
	this.dxy = dxy;
    }

}
