package cz.petrsokola.GpsTransportMode;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GpxParser {

    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document document;
    private LinkedList<TrackPoint> points;

    public GpxParser(File f) throws ParserConfigurationException, SAXException, IOException, ParseException {
	dbFactory = DocumentBuilderFactory.newInstance();
	dBuilder = dbFactory.newDocumentBuilder();
	document = dBuilder.parse(f);
	points = new LinkedList<TrackPoint>();
	parse();
    }

    private void parse() throws ParseException {
	NodeList nl = document.getElementsByTagName("trkpt");
	Node n;
	points.clear();

	TrackPoint trkpt;
	String latS;
	String lonS;
	String timeS;

	int len = nl.getLength();
	for (int i = 0; i < len; i++) {
	    n = nl.item(i);
	    latS = null;
	    lonS = null;
	    timeS = null;
	    if (n.hasAttributes()) {
		latS = n.getAttributes().getNamedItem("lat").getNodeValue();
		lonS = n.getAttributes().getNamedItem("lon").getNodeValue();
	    }
	    if (n.getNodeType() == Node.ELEMENT_NODE && n.hasChildNodes()) {
		NodeList childs = n.getChildNodes();

		for (int j = 0; j < childs.getLength(); j++) {
		    Node m = childs.item(j);
		    if (m.getNodeName().equals("time")) {
			timeS = m.getTextContent();
		    }
		}
	    }
	    double lat = Double.parseDouble(latS);
	    double lon = Double.parseDouble(lonS);
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssX");
	    DateFormat dfms = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSX");
	    Date time = null;
	    try
	    {
	    	time = df.parse(timeS);
	    }
	    catch (ParseException e)
	    {
		try
		{ 
		    time = dfms.parse(timeS); 
		}
		catch (ParseException e2)
		{
		    throw new ParseException("Could not parse the time in the gpx file.", 0);
		}
	    }

	    trkpt = new TrackPoint(lat, lon, time);
	    points.add(trkpt);
	}

    }

    public List<TrackPoint> getPoints() {
	return this.points;
    }

}
