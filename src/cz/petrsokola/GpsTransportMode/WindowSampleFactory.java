package cz.petrsokola.GpsTransportMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import cz.petrsokola.GpsTransportMode.WindowSample.Statistics;
import jdistlib.math.*;
import jdistlib.util.Utilities;

public class WindowSampleFactory {
    public static final int WINDOW_SIZE_IN_SECONDS = 180;

    public LinkedList<WindowSample> getWindowSamples(List<TrackPoint> points) {
	LinkedList<WindowSample> list = new LinkedList<WindowSample>();
	WindowSample sample;
	while ((sample = getOneSample(points)) != null) {
	    list.add(sample);
	}
	return list;
    }

    /*
     * // unboxing done by hand for arrays private double[]
     * unboxDoubles(List<Double> doubles) { double[] arr; int n =
     * doubles.size(); arr = new double[n]; int i; for (i = 0; i<n;i++) arr[i] =
     * doubles.get(i); return arr; }
     */

    // an array of 6 elements: Min, Q1, Median, Mean, Q3, Max
    private void fillUpMinMaxAvg(Statistics s, double[] summary) {
	s.avg = summary[3];
	s.min = summary[1];
	s.max = summary[5];
    }

    private void fillUpStats(Statistics stats, List<Double> samples) {
	double[] arr;

	arr = Utilities.to_double_array(samples);
	// Returns:
	// an array of 6 elements: Min, Q1, Median, Mean, Q3, Max
	fillUpMinMaxAvg(stats, VectorMath.summary(arr));
	stats.stDeviation = VectorMath.sd(arr);
    }

    private WindowSample getOneSample(List<TrackPoint> points) {
	ArrayList<Double> speedVector = new ArrayList<Double>();
	ArrayList<Double> accelerationVector = new ArrayList<Double>();
	ArrayList<Double> turnRateVector = new ArrayList<Double>();

	TrackPoint p;
	Date tStart = null;
	Date tEnd = null;

	int time = 0;
	while (time < WINDOW_SIZE_IN_SECONDS && !points.isEmpty()) {

	    p = points.remove(0);
	    // first pass
	    if (tStart == null)
		tStart = p.getTimestamp();
	    tEnd = p.getTimestamp();

	    speedVector.add(p.getSpeed());
	    accelerationVector.add(p.getAcceleration());
	    turnRateVector.add(p.getTurnRate());
	    time += p.getDt();
	}

	if (time >= WINDOW_SIZE_IN_SECONDS / 2) {
	    WindowSample s = new WindowSample();
	    fillUpStats(s.speed, speedVector);
	    fillUpStats(s.acceleration, accelerationVector);
	    fillUpStats(s.turnRate, turnRateVector);
	    s.startTime = tStart;
	    s.endTime = tEnd;
	    return s;
	} else
	    return null;
    }

}
