package cz.petrsokola.GpsTransportMode;

public enum StatisticsValueKind {
    MIN,
    MAX,
    AVG,
    STDEV
}
